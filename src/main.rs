use color_eyre::eyre;
use serenity::async_trait;
use serenity::framework::standard::macros::{command, group};
use serenity::framework::standard::{CommandResult, StandardFramework};
use serenity::model::channel::Message;
use serenity::prelude::*;
use std::str::FromStr;

mod info;

#[group]
#[commands(roll)]
struct Dnd;

struct Handler;

#[async_trait]
impl EventHandler for Handler {}

#[tokio::main]
async fn main() -> eyre::Result<()> {
    let framework = StandardFramework::new()
        .configure(|c| c.prefix("-"))
        .help(&info::HELP_CMD)
        .group(&DND_GROUP);

    let token = std::env::var("PRUFER_TOK")?;
    let intents = GatewayIntents::non_privileged() | GatewayIntents::MESSAGE_CONTENT;
    let mut client = Client::builder(token, intents)
        .event_handler(Handler)
        .framework(framework)
        .await?;

    client.start().await?;

    Ok(())
}

#[command]
async fn roll(ctx: &Context, msg: &Message) -> CommandResult {
    match msg.content.as_str().split_once('d') {
        Some((lhs, rhs)) => {
            let count = lhs
                .trim()
                .split_once(' ')
                .map_or(1, |(_, count)| u8::from_str(count).unwrap());

            let (max, modifier) = rhs
                .trim()
                .split_once('+')
                .ok_or(eyre::eyre!("invalid format"))?;

            let results = (0..count)
                .map(|_| (rand::random::<f64>() * f64::from_str(max.trim()).unwrap()) as u8 + 1)
                .collect::<Vec<_>>();
            let total = results.iter().sum::<u8>() + u8::from_str(modifier.trim()).unwrap();

            let mut reply = format!("```\nTotal: {total}\n(");
            for (i, n) in results.iter().enumerate() {
                reply += &n.to_string();
                if i != results.len() - 1 {
                    reply += " ";
                }
            }
            reply += ")\n```";
            msg.reply(ctx, reply).await?;
        }
        None => {}
    };

    Ok(())
}
